import Barba from 'barba.js';
import mediumZoom from 'medium-zoom';

export default Barba.BaseView.extend({
  namespace: 'projects',
  onEnter() {
    mediumZoom('[data-zoomable]', {
      background: 'rgba(0,0,0,0.8)',
    });
  },
  onEnterCompleted() {
    const $hero = document.querySelector('.hero');
    const $content = document.querySelector('.content__text');
    const $background = document.querySelector('.technologies__background');
    $hero.classList.add('scale-in');
    $content.classList.add('pull-down');
    $background.style.height = getComputedStyle($hero).height;
  },
  onLeave() {
    const $hero = document.querySelector('.hero');
    const $content = document.querySelector('.content__text');
    $hero.classList.remove('scale-in');
    $content.classList.remove('pull-down');
  },
  onLeaveCompleted() {},
});
