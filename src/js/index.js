import "web-animations-js";
import Barba from "barba.js";
import "../css/main.scss";
import home from "./home";
import project from "./project";

function toggleClass(el, className) {
  if (el.classList) {
    el.classList.toggle(className);
  } else {
    const classes = el.className.split(" ");
    const existingIndex = classes.indexOf(className);

    if (existingIndex >= 0) {
      classes.splice(existingIndex, 1);
    } else {
      classes.push(className);
    }

    el.className = classes.join(" "); //eslint-disable-line
  }
}

const FadeTransition = Barba.BaseTransition.extend({
  start() {
    Promise.all([this.newContainerLoading, this.fadeOut()]).then(
      this.fadeIn.bind(this)
    );
  },

  fadeOut() {
    const animation = this.oldContainer.animate(
      [
        { opacity: getComputedStyle(this.oldContainer).opacity },
        { opacity: 0 }
      ],
      { duration: 400, fill: "forwards" }
    );
    return new Promise(resolve => {
      animation.onfinish = () => {
        resolve();
      };
    });
  },

  fadeIn() {
    const $el = this.newContainer;

    this.oldContainer.style.display = "none";

    $el.style.visibility = "visible";
    $el.style.opacity = 0;

    const animation = $el.animate(
      [{ opacity: getComputedStyle($el).opacity }, { opacity: 1 }],
      { duration: 400, fill: "forwards" }
    );
    animation.onfinish = () => {
      this.done();
    };
  }
});

Barba.Pjax.getTransition = () => FadeTransition;

window.addEventListener("load", () => {
  Barba.Pjax.start();
});

home.init();
project.init();

const menuButton = document.querySelector("#menu");
const sidebar = document.querySelector(".sidebar");
const container = document.querySelector("main.container");
const body = document.querySelector("body");

function toggleMenu() {
  toggleClass(body, "sidebar-active");
  toggleClass(menuButton, "is-active");
}

function closeMenu() {
  if (body.classList.contains("sidebar-active")) {
    toggleMenu();
  }
}

menuButton.addEventListener("click", toggleMenu);
container.addEventListener("click", closeMenu);
sidebar.addEventListener("click", closeMenu);
