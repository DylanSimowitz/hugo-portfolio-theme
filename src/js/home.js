import Barba from 'barba.js';

const $container = document.querySelector('.container');

function scroll() {
  $container.scrollTop -= $container.scrollTop / 4;
  if ($container.scrollTop > 0) {
    window.requestAnimationFrame(scroll);
  }
}

export default Barba.BaseView.extend({
  namespace: 'page',
  onEnter() {
  },
  onEnterCompleted() {

  },
  onLeave() {
    scroll();
  },
  onLeaveCompleted() {
  },
});
