module.exports = {
    "extends": "airbnb",
    "env": {
      "browser": true,
      "node": true
    },
    "plugins": [
        "import"
    ],
    "rules": {
      "jsx-a11y/href-no-hash": "off",
    }
};
