const path = require("path");
const glob = require("glob-all");
const webpack = require("webpack");
const { getIfUtils, removeEmpty } = require("webpack-config-utils");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const StyleLintPlugin = require("stylelint-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const PurifyCSSPlugin = require("purifycss-webpack");

const nodeEnv = process.env.NODE_ENV || "development";
const { ifDevelopment, ifProduction } = getIfUtils(nodeEnv);

module.exports = removeEmpty({
  entry: "./src/js/index.js",

  output: {
    filename: ifProduction("js/[name]-bundle-[hash].js", "js/[name]-bundle.js"),
    path: path.resolve(__dirname, "static"),
    publicPath: "/portfolio"
  },

  node: {
    fs: "empty"
  },

  resolve: {
    alias: {
      masonry: "masonry-layout",
      isotope: "isotope-layout"
    }
  },

  module: {
    rules: [
      {
        test: /\.(sass|scss)$/,
        loader: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader"]
        })
      },
      {
        test: /\.js/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["env"]
          }
        },
        exclude: /node_modules/
      },
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader",
        options: {
          limit: 10000,
          mimetype: "application/font-woff",
          name: "/fonts/[hash].[ext]"
        }
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader",
        options: {
          name: "/fonts/[hash].[ext]"
        }
      }
    ]
  },

  devtool: ifDevelopment("eval-source-map", ""),

  devServer: ifDevelopment({
    host: "0.0.0.0",
    port: 3000,
    proxy: {
      "/": "http://localhost:1313"
    },
    stats: "normal",
    disableHostCheck: true
  }),

  plugins: removeEmpty([
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(nodeEnv)
      }
    }),

    new HtmlWebpackPlugin({
      template: "src/templates/js.ejs",
      inject: false,
      filename: "../layouts/partials/js.html"
    }),

    new HtmlWebpackPlugin({
      template: "src/templates/css.ejs",
      inject: false,
      filename: "../layouts/partials/css.html"
    }),

    ifDevelopment(
      new StyleLintPlugin({
        syntax: "scss",
        emitErrors: false
      })
    ),

    ifProduction(
      new ExtractTextPlugin("css/[name]-bundle-[hash].css"),
      new ExtractTextPlugin("css/bundle.css")
    ),

    ifProduction(new UglifyJSPlugin()),

    ifProduction(
      new PurifyCSSPlugin({
        paths: glob.sync([
          path.join(__dirname, "layouts/**/*.html"),
          path.join(__dirname, "src/js/**/*.js")
        ]),
        minimize: true
      })
    )
  ])
});
